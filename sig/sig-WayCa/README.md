# openEuler WayCa SIG

- WayCa是鲲鹏生态的“硬件使能层”的代表

# 组织会议

- 公开的会议时间：待定

# 成员

- Kenneth-Lee-2012
- joyxu515_admin
- barrysong
- young1c

### Maintainer列表

- Kenneth Lee[@Kenneth-Lee-2012](https://gitee.com/Kenneth-Lee-2012), liguozhu@hisilicon.com
- Wei Xu[@joyxu515_admin](https://gitee.com/joyxu515), xuwei5@huawei.com
- Barry Song[@barrysong](https://gitee.com/barrysong), 21cnbao@gmail.com
- Yicong Yang[@young1c](https://gitee.com/young1c), young.yicong@outlook.com

### Committer列表

- Kenneth Lee[@Kenneth-Lee-2012](https://gitee.com/Kenneth-Lee-2012)
- Wei Xu[@joyxu515_admin](https://gitee.com/joyxu515)
- Barry Song[@barrysong](https://gitee.com/barrysong)
- Yicong Yang[@young1c](https://gitee.com/young1c)

# 联系方式

- [邮件列表](dev@openeuler.org)

# 项目清单

## 项目名称：wayca-scheduler

repository地址：

- https://gitee.com/openeuler/wayca-scheduler

## 项目名称：wayca-scheduler-bench

repository地址：

- https://gitee.com/openeuler/wayca-scheduler-bench

| 分支名                 | 类型         | 说明                                             |
| ---------------------- | ------------ | ------------------------------------------------ |
| master                 | 上游分支同步 | https://github.com/gormanm/mmtests master同步    |
| kp-dev                 | 鲲鹏开发分支 | 增加鲲鹏特定的benchmark配置文件、case            |

## 项目名称：KPL-gmssl

repository地址：

- https://gitee.com/openeuler/KPL-gmssl

## 项目名称：hikptool

repository地址：

- https://gitee.com/openeuler/hikptool
